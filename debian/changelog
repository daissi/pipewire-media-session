pipewire-media-session (0.4.2-2) UNRELEASED; urgency=medium

  * Build new binary packages providing configuration files.
      A wrong combination of pipewire packages can be pulled by
      dependency leading to break audio. See https://bugs.debian.org/1029763
      To avoid this, we need to package these files and adjust dependencies
      on the pipewire side.

 -- Dylan Aïssi <daissi@debian.org>  Mon, 30 Jan 2023 10:55:03 +0100

pipewire-media-session (0.4.2-1) unstable; urgency=medium

  * New upstream (final) release.
     .
     pipewire-media-session is dead and should not be used anymore!
     Please really consider using WirePlumber instead.
     If in doubt, look at the upstream NEWS file.
     .
  * Install upstream NEWS file in /usr/share/doc/pipewire-media-session/
  * Standards-Version: 4.6.2 (no changes needed)
  * Update years in debiam/copyright
  * Bump minimum pipewire to 0.3.44

 -- Dylan Aïssi <daissi@debian.org>  Sat, 21 Jan 2023 13:56:21 +0100

pipewire-media-session (0.4.1-4) unstable; urgency=medium

  * Add patch removing service alias. This alias is only used to prevent
    wireplumber and pipewire-media-session from being enabled at the same time.
    This is already handled at the package level with a conflict between both,
    thus we can safetly remove alias and avoid the issue with systemd.
    See https://bugs.debian.org/997818

 -- Dylan Aïssi <daissi@debian.org>  Fri, 09 Sep 2022 17:38:50 +0200

pipewire-media-session (0.4.1-3) unstable; urgency=medium

  [ Daniel van Vugt ]
  * Configure with-module-sets="" to disable audio by default (LP: #1953052)
     (Closes: #1006364)
    You can reenable it by creating an empty file:
    /usr/share/pipewire/media-session.d/with-pulseaudio
      or/and
    /usr/share/pipewire/media-session.d/with-jack

  [ Dylan Aïssi ]
  * pipewire-media-session is deprecated and should not be used for audio.
    Instead, WirePlumber is the new session manager.
    .
    pipewire-media-session is still installed by default with pipewire
    to allow only screen-sharing, which is not possible with WirePlumber
    without editing config files.

 -- Dylan Aïssi <daissi@debian.org>  Tue, 31 May 2022 22:10:49 +0200

pipewire-media-session (0.4.1-2) unstable; urgency=medium

  * Add a conflict with wireplumber as recommended by upstream.

 -- Dylan Aïssi <daissi@debian.org>  Mon, 08 Nov 2021 11:42:53 +0100

pipewire-media-session (0.4.1-1) unstable; urgency=medium

  * New upstream release

 -- Dylan Aïssi <daissi@debian.org>  Wed, 03 Nov 2021 09:55:43 +0100

pipewire-media-session (0.4.0-1) unstable; urgency=medium

  * Initial release (Closes: #997001)
  * Improve d/copyright according to the ftpmasters' comments.

 -- Dylan Aïssi <daissi@debian.org>  Wed, 27 Oct 2021 22:22:02 +0200
