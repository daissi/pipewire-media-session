# Valentin Laskov <laskov@festa.bg>, 2016. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2020-10-15 21:30+0000\n"
"Last-Translator: Emanuil Novachev <em.novachev@gmail.com>\n"
"Language-Team: Bulgarian <https://translate.fedoraproject.org/projects/"
"pipewire/pipewire/bg/>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.2.2\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr ""

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr ""

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr ""
